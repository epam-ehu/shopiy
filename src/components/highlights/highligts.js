import "./highligts.css";

export default {
  setActiveItem(number) {
    const element = document.querySelector(
      `.highligts-list > :nth-child(${number})`
    );

    if (element) {
        element.classList.toggle('active');
    }
  },

  render() {
    const container = document.createElement("ul");

    container.classList.add("highligts-list");

    const data = [
      {
        title: "SEO optimized, high Google work",
        description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      },
      {
        title: "Fast loading, low bounce rates",
        description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      },
      {
        title: "SEO optimized, high Google work",
        description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      },
    ];

    data.forEach((item) => {
      const li = document.createElement("li");
      li.classList.add("highligts-list-item");
      li.innerHTML = `
        <h4>${item.title}</h4>
        <p>${item.description}</p>
        `;

      container.append(li);
    });

    return container;
  },
};
