import "./styles.css";

import { createHeader, getElement } from "./components/header/header";
import { Hero } from "./components/hero/hero";
import Features from "./components/features/features";
import Highligts from "./components/highlights/highligts";

createHeader();

const hero = Hero.render();
document.querySelector("#app").append(hero);

// const feature = await Features.render();
// document.querySelector("#app").append(feature);

document.querySelector("#app").append(Highligts.render());

let activeElem = 1;
setInterval(() => {
  Highligts.setActiveItem(activeElem);
  activeElem++;

  if (activeElem > 3) {
    activeElem = 1;
  }
}, 500);
