export const Hero = {
  render: () => {
    const hero = document.createElement("div");

    hero.classList.add("hero");

    hero.innerHTML = `
              <h1>Power up your<br>
              shopify products</h1>
          `;

    return hero;
  },
};
