import featuresData from "./features.json";

export class FeaturesService {
  #maxLimit = 10;

  /**
   *
   * @param {number} limit amount of features to load
   */
  getFeatures(limit = 2) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (limit > this.#maxLimit) {
          reject(new Error("Extended max limit"));
        }

        const features = featuresData.slice(0, limit);

        resolve(features);
      }, 100);
    });
  }
}

export default {
  async render() {
    const featuresService = new FeaturesService();

    const featuresContainer = document.createElement("article");

    featuresContainer.classList.add("features-container");

    const info = document.createElement("div");
    info.innerHTML = `
          <h2>Our creative process for your business</h2>
        `;

    const featuresGroup = document.createElement("ul");
    const featuresGroupData = (await featuresService.getFeatures(4)).map(
      (feature) => {
        const li = document.createElement("li");
        li.innerHTML = `
                    <h4>${feature.title}</h4>
                    <p>${feature.description}</p>
                `;
        return li;
      }
    );

    console.info(featuresGroupData);

    featuresGroupData.forEach((featureElement) =>
      featuresGroup.append(featureElement)
    );

    console.info(featuresGroup);

    featuresContainer.append(info);
    featuresContainer.append(featuresGroup);

    return featuresContainer;
  },
};
